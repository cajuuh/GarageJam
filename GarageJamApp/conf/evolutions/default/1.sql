# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table anunciante (
  id                        bigint auto_increment not null,
  nome                      varchar(255),
  cidade                    varchar(255),
  bairro                    varchar(255),
  is_banda                  boolean,
  constraint pk_anunciante primary key (id))
;

create table anuncio (
  id                        bigint auto_increment not null,
  TITULO                    varchar(255),
  DESCRICAO                 varchar(255),
  constraint pk_anuncio primary key (id))
;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists anunciante;

drop table if exists anuncio;

SET REFERENTIAL_INTEGRITY TRUE;

