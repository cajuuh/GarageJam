package controllers;

import play.data.Form;
import play.db.ebean.Model;
import play.db.jpa.Transactional;
import play.mvc.*;

import views.html.*;

import com.google.common.collect.Iterables;

import java.util.*;

import static play.libs.Json.toJson;

public class Application extends Controller
{
    public static Result index()
    {
        return ok(index.render("Application ready!"));
    }
}
