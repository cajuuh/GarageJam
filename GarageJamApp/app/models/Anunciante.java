package models;

import com.sun.javafx.UnmodifiableArrayList;

import javax.persistence.*;
import java.util.*;


/**
 * Created by Pedro on 02/06/2015.
 */
@Entity
public class Anunciante
{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column
    private String nome;
    @Column
    private String cidade;
    @Column
    private String bairro;
    @Column
    private boolean isBanda;

    @OneToMany
    @ElementCollection
    @CollectionTable(name = "INSTRUMENTOS", joinColumns = @JoinColumn(name="anunciante_id"))
    @Column(name="INSTRUMENTOS_NOME")
    private List<String> instrumentos;

    public Anunciante()
    {
        instrumentos = new ArrayList<String>();
    }

    public Anunciante(String nome, String cidade, String bairro, List<String> instrumentos, List<String> estilosLike, List<String> estilosDislike, boolean isBanda)
    {
        setNome(nome);
        setCidade(cidade);
        setBairro(bairro);
        setIsBanda(isBanda);
    }

    public void setInstrumentos(List<String> instrumentos)
    {
        this.instrumentos = instrumentos;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public void setCidade(String cidade)
    {
        this.cidade = cidade;
    }

    public void setBairro(String bairro)
    {
        this.bairro = bairro;
    }

    public void setIsBanda(boolean isBanda)
    {
        this.isBanda = isBanda;
    }

    public String getNome()
    {
        return nome;
    }

    public String getCidade()
    {
        return cidade;
    }

    public String getBairro()
    {
        return bairro;
    }

    public boolean isBanda()
    {
        return isBanda;
    }

    public List<String> getInstrumentos()
    {
        return instrumentos;
    }

    private void addInstrumento(String instrumento)
    {
        this.instrumentos.add(instrumento);
    }
}
