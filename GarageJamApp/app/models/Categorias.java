package models;

/**
 * Created by Pedro on 18/05/2015.
 */
public enum Categorias
{
    VOCAIS,
    CORDAS,
    PERCUSSAO,
    ELETRONICO;
}
