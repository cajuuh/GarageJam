package models;

/**
 * Created by Pedro on 20/05/2015.
 */
public class InvalidConstructorException extends Exception
{
    public InvalidConstructorException()
    {
        super();
    }

    public InvalidConstructorException(String message)
    {
        super(message);
    }

    public InvalidConstructorException(String message, Throwable cause)
    {
        super(message, cause);
    }
    public InvalidConstructorException(Throwable cause)
    {
        super(cause);
    }
}
