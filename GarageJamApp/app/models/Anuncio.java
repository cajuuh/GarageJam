package models;

import javax.persistence.*;
import java.util.*;

/**
 * Created by Pedro on 02/06/2015.
 */

@Entity
public class Anuncio
{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name="TITULO")
    private String titulo;
    @Column(name="DESCRICAO")
    private String descricao;
    @OneToMany
    @JoinTable(name="ANUNCIANTE")
    private Collection<Anunciante> anunciantes;

    public Anuncio()
    {
        anunciantes = new ArrayList<Anunciante>();
    }

    public Anuncio(String titulo, String descricao)
    {
        this();
        setTitulo(titulo);
        setDescricao(descricao);
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public String getDescricao()
    {
        return descricao;
    }
}
